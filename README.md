# multi-image-project

## 项目简介

## 开发指南

开发使用 Python 3.7。

1. 在 IDE 中启用 flake8 代码查错工具和 yapf 代码格式化工具。保持默认设置。

   - 代码格式遵循 [PEP8](https://www.python.org/dev/peps/pep-0008/#code-lay-out)。
   - 类（Class）名首字母大写：`FilterFactory`。
   - 函数与变量名使用 snake_case： `src_image`。
   - 内部函数与变量以单下划线开始：`_single_leading_underscore`。
   - 禁止 \* 引用：`from .util import *`。

2. 将所有依赖库（包含 c2v）保存在 `requirements.txt` 中，并使用 pip 安装 `pip install -r requirements.txt`。
3. 算法配置文件保存在 `multi-image-project/configs` 中，模板文件保存在 `multi-image-project/templates`。

## 测试

1. 测试图片保存在 `samples` 中。
2. 修改 `test.py`。
3. 运行 `python test.py`。

## 部署算法

1. 安装 sirius-cli `pip install git+https://gitlab.com/c2v/plugins-v2/sirius-cli.git`。
2. 将 `PYSOCK_HOST` 环境变量设置为后台 IP 地址，默认为 `localhost`。
3. 在项目文件夹中运行 `sir deploy multi-image-project`。

详细信息见 `sir --help`。


>>from .mr import mapreduce
>>from .utils import timed
import cv2
import c2v


# 整合所有`RequestID`(map)处理的结果,返回最终结果

```
def reducer(task_id: str, results: [dict], meta: dict = None):

    processing_times = [
        r['info']['ProcessingTime'] for r in results
        if 'ProcessingTime' in r['info']
    ]
    results_codes = [r['result'] for r in results]
    annotations = [r['annotation'] for r in results]
    
    ############################################
    # consolidate results of all images
    ############################################
    
    results_all = {
        'result': 1 if all(c == 1 for c in results_codes) else 0,
        'annotation': annotations,
        'info': {
            'TaskID': task_id,
            'Requests': results_codes,
            'MaxTime': max(processing_times),
            'MinTime': min(processing_times),
        },
    }
    print(results_all)
    return results_all
```
# 单独处理每一个`RequestID`(单图)，返回结果必须是dict类型

```
@timed
def mapper(image_path: str, index: int, meta: dict = None):
    res = {'result': 1,
           'annotation': [],
           'info': {}}

    ############################################
    # process the image
    # #Example
    ############################################
    annotations = []

    img = cv2.imread(image_path, 0)
    # img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    blur = cv2.GaussianBlur(img, (3, 3), 0)
    ret, th = cv2.threshold(blur, 124, 255, cv2.THRESH_BINARY_INV)

    contours, h = cv2.findContours(th, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    result = 1
    for con in contours:
        area = cv2.contourArea(con)
        if area > 20:
            bound = cv2.minAreaRect(con)
            annotations.append(
                c2v.createAnnotationRotatedRectangle(bound[0], bound[1][0], bound[1][1], bound[2], (0, 0, 255),
                                                     2))
            result = 0
            break

    res['result'] = result
    res['annotation'] = annotations
    print('index:', index, res)
    return res
```
