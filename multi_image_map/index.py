from .mr import mapreduce
from .utils import timed
import cv2
import c2v


def reducer(task_id: str, results: [dict], meta: dict = None):

    processing_times = [
        r['info']['ProcessingTime'] for r in results
        if 'ProcessingTime' in r['info']
    ]
    results_codes = [r['result'] for r in results]
    annotations = [r['annotation'] for r in results]
    ############################################
    # consolidate results of all images
    ############################################
    results_all = {
        'result': 1 if all(c == 1 for c in results_codes) else 0,
        'annotation': annotations,
        'info': {
            'TaskID': task_id,
            'Requests': results_codes,
            'MaxTime': max(processing_times),
            'MinTime': min(processing_times),
        },
    }
    print(results_all)
    return results_all


@timed
def mapper(image_path: str, index: int, meta: dict = None):

    res = {'result': 1,
           'annotation': [],
           'info': {}}

    ############################################
    # process the image
    # #Example
    ############################################
    annotations = []

    img = cv2.imread(image_path, 0)
    # img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    blur = cv2.GaussianBlur(img, (3, 3), 0)
    ret, th = cv2.threshold(blur, 124, 255, cv2.THRESH_BINARY_INV)

    contours, h = cv2.findContours(th, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    result = 1
    for con in contours:
        area = cv2.contourArea(con)
        if area > 20:
            bound = cv2.minAreaRect(con)
            annotations.append(
                c2v.createAnnotationRotatedRectangle(bound[0], bound[1][0], bound[1][1], bound[2], (0, 0, 255),
                                                     2))
            result = 0
            break

    res['result'] = result
    res['annotation'] = annotations
    print('index:', index, res)

    return res


main = mapreduce(mapper, reducer)
