import collections
from functools import partial, wraps
from multiprocessing.pool import ThreadPool as Pool
from pathlib import Path

from . import store

from .utils import timed, wait_for_request


def reduce_wrapper(reducer, input):
    task_id, results, meta = input
    res = reducer(task_id, results, meta=meta)
    return task_id, res


def map_wrapper(mapper, input):
    task_id, request_id, meta = input

    request = wait_for_request(request_id)

    res = {'result': 0, 'info': {}}

    if request is not None:
        image_path, *args = request
        res = mapper(image_path, *args, meta=meta)
    else:
        res['result'] = 408
    return task_id, res


def append_meta(inputs, meta):
    return [(*item, meta) for item in inputs]


def mapreduce(mapper, reducer):
    """Processes requests with the Map-Reduce framework.

    Args:
        mapper (Callable): An algorithm called on each image.
        reducer (Callable): Consolidation function.

    Returns:
        Callable: The main function.
    """
    _mapper = partial(map_wrapper, mapper)
    _reducer = partial(reduce_wrapper, reducer)

    def partition(mapped_values):
        """ Group the mapped values by their key.

        Args:
            mapped_values ([(k, val)]): List of key-value pairs.

        Returns:
            [(k, [val])]: An unsorted sequence of tuples with a key and a sequence of values.
        """
        partitioned_data = collections.defaultdict(list)
        for key, value in mapped_values:
            partitioned_data[key].append(value)
        return partitioned_data.items()

    def execute(inputs, meta, pool, chunksize=1):
        """Process the inputs through the given map and reduce functions.

        Args:
            inputs (Iterable[S]): An iterable containing the input data to be processed. Each datum is a key-value pair.
            meta (ditc): The meta object of the call.
            chunksize (int, optional): The portion of the input data to hand to each worker.  This can be used to tune performance during the mapping phase. Defaults to 1.

        Returns:
            List[T]: A sequence of tuples with a key and a value produced by the reduce phase.
        """

        inputs = append_meta(inputs, meta)
        map_responses = pool.map(_mapper, inputs, chunksize=chunksize)
        partitioned_data = partition(map_responses)
        partitioned_data = append_meta(partitioned_data, meta)
        reduced_values = pool.map(_reducer, partitioned_data)
        return dict(reduced_values)

    @wraps(mapper)
    @timed
    def main(image_path: str, *args, meta: dict):
        """Replace the main function. Return 202 if meta['Requests] is None or process all requests in parallel with the mapper function, and consolidate mapped results with the reducer function.
        
        Args:
            image_path (str): An image path.
            meta (dict): An object containing meta information.
        
        Returns:
            dict: An algorithm response.
        """

        task_id = meta['TaskID']
        requests = meta.get('Requests', [])
        request_id = meta['RequestID']
        #print(meta)
        store.set(request_id, [image_path, *args])

        if len(requests) == 0:
            res = {
                "result": 202,
                "info": {
                    "Image": image_path,
                    "TaskID": task_id,
                }
            }

        else:
            pool = Pool(len(requests))
            meta['_ImageFolder'] = Path(image_path).parent.resolve()
            req_count = len(requests)

            requests = zip([task_id] * req_count, requests)
            res = execute(requests, meta, pool).get(task_id, None)

        return res

    return main