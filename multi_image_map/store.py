import json
from json import JSONDecodeError
import tempfile
from pathlib import Path
import time

tmpdir = tempfile.TemporaryDirectory()
STORE = Path(tmpdir.name)


def set(k: str, data: object):

    entry = STORE / f'{k}.json'
    with open(entry, 'w') as f:
        json.dump(data, f)


def get(k: str):
    entry = STORE / f'{k}.json'
    try:
        with open(entry) as f:
            try:
                data = json.load(f)
            except JSONDecodeError:
                # in case of w/r race condition, try again after 1ms (writing takes less than 1ms)
                time.sleep(0.001)
                try:
                    data = json.load(f)
                except JSONDecodeError:
                    data = None
            return data
    except FileNotFoundError:
        return None
