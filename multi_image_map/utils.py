# all functions go here
import time
from functools import wraps
from pathlib import Path

from . import store

import c2v


def timed(f):
    @wraps(f)
    def wrapper(*args, **kargs):
        start = time.time()
        res = f(*args, **kargs)
        end = time.time()
        duration = float((end - start) * 1000)
        if res is not None:
            info = res.get('info', {})
            info['ProcessingTime'] = duration
            res['info'] = info
        return res

    return wrapper


def wait_for_request(request_id: str, timeout=0.5):
    """Get parameters of a request. Block until the request is ready or timeout.

    Args:
        request_id (str): The request id to which the image belongs.
        timeout (int): Max number of seconds to wait. Defaults to 1.
    """

    interval = 1 / 1000
    start = time.time()
    args = store.get(request_id)
    while args is None and time.time() - start <= timeout:
        args = store.get(request_id)
        time.sleep(interval)
    return args


def load_config():
    """Load json configuration from configs/config.json.

    Example:
    ```
    config = load_config()
    if config.debug:
      print("Debug on")
    ```
    """
    base = Path(__file__).parent.resolve()
    config_path = str(base / "configs" / "config.json")

    with open(config_path, "r") as content:
        config = c2v.json2obj(content.read())
    return config
