import os
from os import path
from setuptools import setup, find_packages
import re

HERE = path.abspath(path.dirname(__file__))
PACKAGES = find_packages()
META_PATH = path.join('multi-image-project', '__init__.py')

with open(path.join(HERE, 'LICENSE'), encoding='utf-8') as f:
    LICENSE = f.read()


def find_meta(meta, meta_path=META_PATH):
    """
    Extract __*meta*__ from META_FILE.
    """
    with open(path.join(HERE, meta_path), encoding='utf-8') as f:
        META_FILE = f.read()
    meta_match = re.search(
        r"^__{meta}__ = ['\"]([^'\"]*)['\"]".format(meta=meta),
        META_FILE, re.M
    )
    if meta_match:
        return meta_match.group(1)
    raise RuntimeError("Unable to find __{meta}__ string.".format(meta=meta))


# Get the long description from the README file
with open(path.join(HERE, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

# get the dependencies and installs
requirement_path = path.join(HERE, 'requirements.txt')
if path.isfile(requirement_path):
    with open(requirement_path) as f:
        install_requires = [x.strip()
                            for x in f.read().splitlines() if not x.startswith('#')]


setup(
    name='multi-image-project',
    version=find_meta("version"),
    description='Please give a description',
    long_description=long_description,
    license=LICENSE,
    classifiers=[
        'Programming Language :: Python :: 3.7',
    ],
    keywords='',
    packages=PACKAGES,
    include_package_data=True,
    author='Sigma Squares Ltd.',
    install_requires=install_requires,
    options={"bdist_wheel": {"universal": "1"}}
)
