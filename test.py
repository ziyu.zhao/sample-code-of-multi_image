# Sample Test passing with nose and pytest
import sys
import os
import json
from multi_image_map.index import main


def check_result(result):
    """The return result should have a result field of type int and a info field of type dict.

    Arguments:
        result {dict} -- Return result from main().
    """
    assert isinstance(result["result"], int)
    assert isinstance(result["info"], dict)
    return result


def get_sample_images():
    images = []
    # return image paths from the "samples" dir
    sample_dir = os.path.join(os.path.abspath(
        os.path.dirname(__file__)), "samples")
    files = []

    # r=root, d=directories, f = files
    for root, d, files in os.walk(sample_dir):
        for file in files:
            # optionally filer file based on extensions
            images.append(os.path.join(root, file))

    return images


def test_with_sample_images():
    images = get_sample_images()

    for img in images:
        print(img)

    requests = range(0, len(images), 1)

    results = (main(img, i, meta={"TaskID": 1, "RequestID": i, "Requests": requests if i == len(images) - 1 else []})
               for i, img in enumerate(images))

    checked_results = (check_result(r) for r in results)

    json_results = [json.dumps(r, sort_keys=True, indent=4) for r in checked_results]

    return json_results


if __name__ == "__main__":
    test_with_sample_images()
    # print(r)
